import os
import pathlib
import shutil

from app import gitoperations as gitop
from app import cli


# def test_clone():
#     repo = gitop.GitOperations('git@gitlab.com:selin-gungor/dockerci.git')
#
#     temp_dir = os.path.join(gitop._root_path(), 'public')
#     pathlib.Path(temp_dir).mkdir(parents=True, exist_ok=True)
#
#     result = repo.clone_to(temp_dir)
#     assert result is not None
#     shutil.rmtree(temp_dir)
#
#
# def test_public_repo():
#     temp_dir = os.path.join(gitop._root_path(), 'tmp_public')
#     pathlib.Path(temp_dir).mkdir(parents=True, exist_ok=True)
#     result = cli.cli('git@gitlab.com:selin-gungor/dockerci.git', temp_dir)
#     assert result is not None
#     shutil.rmtree(temp_dir)


def test_private_repo():
    temp_dir = os.path.join(gitop._root_path(), 'tmp_private')
    pathlib.Path(temp_dir).mkdir(parents=True, exist_ok=True)
    # try:
    result = cli.cli('git@gitlab.com:selin-gungor/dockergen.git', temp_dir)
    assert result is not None
    shutil.rmtree(temp_dir)
