### Installation

```bash 
pipenv install
pipenv shell
python setup.py develop
```
### Running the application from command line
```bash 
easygit --url 'git_address' --path 'where_to_clone'
```


### Example
Do not forget to set $SSH_KEY!
```bash 
easygit --url 'git@gitlab.com:selin-gungor/dockerci.git' --path '/target_path'
```