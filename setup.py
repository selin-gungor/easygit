from setuptools import setup

setup(
    name='easygit',
    version='0.0.1',
    author="Selin Gungor",
    entry_points={
        'console_scripts': [
            'easygit = app.cli:main',
        ],
    }
)
