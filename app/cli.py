import logging as log

import fire

from app.gitoperations import GitOperations


def cli(url, path):
    repo = GitOperations(url)
    cloned_repo = repo.clone_to(path)
    log.info('Yay!!!')
    return cloned_repo


def main():
    fire.Fire(cli)
