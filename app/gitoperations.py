import logging as log
import os
import pathlib
import subprocess
from os.path import expanduser

from git import Git
from git import Repo


def _root_path():
    root_path = os.path.dirname(os.path.abspath(__file__))
    return root_path


class GitOperations:
    def __init__(self, url: str):
        self.url = url

    def clone_to(self, target_location, branch='master') -> str:
        repo_name = self._get_git_repo_name()
        try:
            cloned_repo = Repo.clone_from(self.url, target_location, branch=branch)
            return cloned_repo
        except:
            log.info('Project is not public, trying with ssh key..')
            temp_dir = os.path.join(_root_path(), 'tmp')
            pathlib.Path(temp_dir).mkdir(parents=True, exist_ok=True)

            home_path = expanduser("~")
            ssh_path = os.path.join(home_path, '.ssh')
            file_path = os.path.join(ssh_path, 'id_rsa')
            os.environ["ID_RSA"] = file_path

            os.makedirs(ssh_path, exist_ok=True)
            with open(file_path, "w") as ssh_key_file:
                ssh_key_file.write(str(os.getenv('SSH_KEY')))
            os.chmod(file_path, 0o600)

            known_hosts_path = os.path.join(ssh_path, 'known_hosts')
            # with open(known_hosts_path, "w") as known_hosts_file_handler:
            #     known_hosts_file_handler.write("")
            os.environ["KNOWN_HOSTS"] = known_hosts_path

            os.environ["SSH_DIR"] = ssh_path

            setup_ssh_file = os.path.join(_root_path(), 'setup_ssh.sh')
            subprocess.call(setup_ssh_file, shell=True)

            run_ssh_file = os.path.join(_root_path(), "run_ssh.sh")
            with Git().custom_environment(GIT_SSH=run_ssh_file):
                cloned_repo = Repo.clone_from(self.url, target_location, branch=branch)

        return cloned_repo

    def _get_git_repo_name(self):
        repo_name = self.url.split("/")[-1].split(".")[0]
        return repo_name
